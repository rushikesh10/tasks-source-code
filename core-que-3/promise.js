
console.log('Promise Chaining Started ****');

getUser(69)
.then( user => getService(user) )
.then( services => getServiceCost(services) )
.then( result => console.log(result)  )
.catch(err => console.log(err));

console.log('Promise Chaining Ended ****');


function getUser(userID){
    return new Promise( (resolve,reject) => {
        console.log('Getting user from database...');
        setTimeout( () => {
            resolve({user_id:userID,username:'Rushikesh'})
            // reject(new Error('Something went wrong 1...'));
        } , 1000)
    });
}

function getService(user){
    return new Promise( (resolve,reject) => {
        console.log(`Getting the services list for user ${user.username}...`);
        setTimeout( () => {
            resolve(['Email','VPN','CDN'])
            // reject(new Error('Something went wrong 2...'));
        },1000)
    });
}

function getServiceCost(services){
    return new Promise( (resolve,reject) =>  {
        console.log(`calculatiing cost of ${services} services...`);
        setTimeout( () => {
            resolve(`Total Cost is ${services.length * 1000}`)
        },1000 )
    });
}


