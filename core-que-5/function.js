function fahrenheitToCelsius(fahrenheit){
    let result = (5/9)*(fahrenheit-32);
    console.log(`degree celsius from normal function is ${result.toFixed(2)}`);
}

fahrenheitToCelsius(69);

const anonymous1 = function(fahrenheit) {
    let result = (5/9)*(fahrenheit-32);
    console.log(`degree celsius from anonymous function is ${result.toFixed(2)}`);
}

anonymous1(69);

const anonymous2 = (fahrenheit) => {
    let result = (5/9)*(fahrenheit-32);
    console.log(`degree celsius from arrow function is ${result.toFixed(2)}`);
}

anonymous2(69);


(function(fahrenheit){
    let result = (5/9)*(fahrenheit-32);
    console.log(`degree celsius from iife is ${result.toFixed(2)}`);
})(69);