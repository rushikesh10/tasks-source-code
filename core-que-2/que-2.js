const fs = require('fs');

const file = './que-2-file.txt';

const errorFirstCallbackFunction = (error,data) => {
    if(error){
        return console.log(error);
    }
    console.log("File Successfully Executed");
    console.log(`File content is \n${data.toString()}`);
}

fs.readFile(file,errorFirstCallbackFunction);
