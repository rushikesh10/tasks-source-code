const express = require('express');
const mongoose  = require('mongoose');
const customers = require('./routes/customers');

const app = express();

mongoose.connect('mongodb://localhost/mongo-exercises')
    .then( ()=> console.log('connected to mongodb...') )
    .catch ( (error) => console.log(error) )

app.use(express.json());
app.use('/api/customers', customers);

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));