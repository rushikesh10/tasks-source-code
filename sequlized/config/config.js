const {Sequelize,DataTypes} = require('sequelize');

const sequelize = new Sequelize('test','root','',{
    host:'localhost',
    dialect:'mysql',
    pool:{max:5,min:0,idel:10000}
})

sequelize.authenticate()
    .then( () => console.log('connected') )
    .catch( (error) => console.log("mysql Error",error))

const db = {}
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.user = require('../models/user')(sequelize,DataTypes);

db.sequelize.sync()
    .then( () => console.log("yes re sync") )
    .catch( (error) => console.log(error) );