
module.exports = (sequelize,DataTypes) => {
    const User = sequelize.define("user",{
       name:DataTypes.STRING,
       email:{
           type:DataTypes.STRING,
           defaultValue:'test@gmail.com'
       },
       gender:{
           type : DataTypes.STRING
       } 

    });
}