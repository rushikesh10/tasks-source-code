const mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: 'products'
});

con.connect(function(err) {
  try {
    if (err) throw err;
    console.log("Connected!");
  } catch (error) {
    res.status(400).send(error.message);
  }
});

module.exports = con;