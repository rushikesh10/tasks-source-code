const express = require('express')
const route = express.Router()
const {getAllProducts,getSingleProduct,deleteProduct,insertProduct,editProduct} = require('../controllers/products')

route.get('/',getAllProducts);
route.get('/:id',getSingleProduct);
route.delete('/:id',deleteProduct);
route.post('/',insertProduct);
route.put('/:id',editProduct);

module.exports = route;