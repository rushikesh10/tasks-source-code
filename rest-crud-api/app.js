const express = require('express');
const app = express();
const products = require('./routes/products')

app.use(express.json());

app.use('/api/products',products);

app.listen(3000,()=>{
    console.log(`server is listening on port 3000...`);
});