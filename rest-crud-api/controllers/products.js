
const conn = require('../config/config')

const getAllProducts = (req,res) =>{
    try {
        conn.query('SELECT * FROM item', (err,rows) => {
            if(err) throw err('something went wrong');
            //console.log('Data received from Db:');
            res.status(200).send(rows);
          });
    } catch (error) {
        res.status(400).send(error.message);
    }
};

const getSingleProduct = (req,res) =>{
    try {
        conn.query('SELECT * FROM item where id = '+req.params.id, (err,rows) => {
            if(err) throw err('something went wrong');
            //console.log('Data received from Db:');
            res.status(200).send(rows);
          });
    } catch (error) {
        res.status(400).send(error.message);
    }
};

const deleteProduct = (req,res) =>{
    try {
        conn.query('delete FROM item where id = '+req.params.id, (err,rows) => {
            if(err) throw err('something went wrong');
            //console.log('Data received from Db:');
            res.status(200).send(rows);
          });
    } catch (error) {
        res.status(400).send(error.message);
    }
};

const insertProduct = (req,res) => {
    try { 
        conn.query(`insert into item(name,category,price) values('${req.body.name}', '${req.body.category}',${req.body.price})`,(err,rows)=>{
            if(err) throw err('Something went wrong...');
            res.status(200).send(rows);
        });
    } catch (error) {
        res.status(400).send(error.message);
    }
};

const editProduct = (req,res) => {
    try {
        conn.query(`update item set ? where ? `,[{name:req.body.name,category:req.body.category,price:req.body.price},{id:req.params.id}],(err,rows)=>{
            if(err) throw err('Something went wrong...');
            res.status(200).send(rows);
        });
    } catch (error) {
        res.status(400).send(error.message);
    }
};

module.exports = {getAllProducts,getSingleProduct,deleteProduct,insertProduct,editProduct}