const nodemailer = require('nodemailer')

const transporter = nodemailer.createTransport({
    service:'gmail',
    auth:{
        user:'youemail@mail.com',
        pass:'yourpassword'
    }
});

const mailOptions = {
    from:'youemail@mail.com',
    to:'rushikesh@nimapinfotech.com,rushikeshdhamankar@gmail.com',
    subject:'sending mail using node.JS',
    text:'Hello this is body of email',
};

transporter.sendMail(mailOptions,function(error,info){
    if(error){
        console.log(error.message);
    }else{
        console.log('Email Sent : '+info.response);
    }
})