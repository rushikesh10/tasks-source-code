const hbs = require('nodemailer-express-handlebars')
const nodemailer = require('nodemailer')
const path = require('path')

// initialize nodemailer
var transporter = nodemailer.createTransport(
    {
        service: 'gmail',
        auth:{
            user:'youemail@mail.com',
            pass:'yourpassword'
        }
    }
);

// point to the template folder
const handlebarOptions = {
    viewEngine: {
        partialsDir: path.resolve('./views/'),
        defaultLayout: false,
    },
    viewPath: path.resolve('./views/'),
};

// use a template file with nodemailer
transporter.use('compile', hbs(handlebarOptions))


var mailOptions = {
    from: 'youemail@mail.com', 
    to: 'rushikesh@nimapinfotech.com', 
    subject: 'Welcome Email!',
    template: 'email', 
    context:{
        name: "Rushikesh", 
        tableBody: getMailData()
    }
};

// trigger the sending of the E-mail
transporter.sendMail(mailOptions, function(error, info){
    if(error){
        return console.log(error);
    }
    console.log('Message sent: ' + info.response);
});

function getMailData(){
    const currentDate = new Date();
    let result = []; 
    for (let i = 1; i <= 5; i++) {
        const previousDate = new Date(currentDate.setDate(currentDate.getDate() - 1));
        result.push({srNo:i,date:previousDate})  
    }
    return result;
}