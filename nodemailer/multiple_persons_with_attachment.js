const nodemailer = require('nodemailer')

const transporter = nodemailer.createTransport({
    service:'gmail',
    auth:{
        user:'youemail@mail.com',
        pass:'yourpassword'
    }
});

// NOTE : if we use both text and html then it selects by default html and put it into mail body

const mailOptions = {
    from:'youemail@mail.com',
    to:'rushikesh@nimapinfotech.com,rushikeshdhamankar@gmail.com',
    subject:'sending mail using node.JS',
    text:'Hello this is body of email',
    html:'<h1>GeeksforGeeks</h1><p>I love geeksforgeeks</p>',
    attachments: [{
        filename: 'attachment.txt',
        path: './demo_file.txt'
    }]
};

transporter.sendMail(mailOptions,function(error,info){
    if(error){
        console.log(error.message);
    }else{
        console.log('Email Sent : '+info.response);
    }
})