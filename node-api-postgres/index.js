const express = require('express')
const bodyParser = require('body-parser')
const db = require('./queries')
const app = express()
const port = 3000

app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended:true,
    })
)

// app.use(  (req,res,next) => {
//     const age = req.params.id;
//     if( age <= 10 ){
//         return res.status(400).send("unauthorized");
//     }
//     else if( age > 10 && age < 18 ){
//         return res.status(200).send("age not allowed");
//     }
//     next();
// })

// app.get('/getdata/:age',(request,response)=>{
//     const age = request.params.age;
//     return response.status(200).json({msg:"success",age});
//     // response.json({info:"Node JS and postgre API"})
// })

app.get('/users',db.getUsers);
app.get('/users/:id',db.getUserById);
app.delete('/users/:id',db.deleteUser);
app.put('/users/:id',db.updateUser);
app.post('/users',db.createUser);


app.listen(port , ()=>{
    console.log(`App is running on port ${port}`);
})