const express = require('express');
const index_route = require('./routes/time_zone')
const app = express();

app.use('/api',index_route);

app.use(function(err, req, res, next) {
    res.status(500).send(err);
});

const port = 3000;
app.listen(port,()=>{
    console.log(`server is running on port ${port}`);
})