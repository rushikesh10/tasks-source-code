const express = require('express');
const route = express.Router();
const {get_current_time_five_zones,get_current_time_zone,get_specific_time_zone,get_time_zone_list} = require('../controllers/time_zone');

const isRegion = require('../middlewares/isRegion')

route.get('/get-all-time-zones',get_time_zone_list);
route.get('/get-current-time-zone',get_current_time_zone);
route.get('/get-specific-time-zone/:value1/:value2',isRegion,get_specific_time_zone);
route.get('/get-current-time-in-five-zones',get_current_time_five_zones);


module.exports = route;