const moment = require('moment');
require('moment-timezone');

module.exports = function isRegion(req,res,next) {
    try {
        const id = req.params.value1+'/'+req.params.value2;
        const time_zone_list = moment.tz.names();
        console.log('Performing validations from middleware');
        if(!id){
            throw Error("Please provide time zone");
        }
    
        if(!time_zone_list.includes(id)){
            throw Error("Correct time zone require");
        }
    
        req.query.validation_status = 'All Valditions OK'
        next();
    } catch (error) {
        res.status(500).json({status:'failure',message:error.message})
    }
}