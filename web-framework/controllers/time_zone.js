const moment = require('moment');
require('moment-timezone');

exports.get_time_zone_list = (req,res) =>{
    const list = moment.tz.names();
    res.status(200).send(list);
}

exports.get_current_time_zone = (req,res) =>{
    const date_and_time = moment();
    res.status(200).send(`Current time as per Indian time zone :  ${date_and_time}`);
}

exports.get_specific_time_zone = (req,res) =>{
    const region = req.params.value1+'/'+req.params.value2;
    const validation_status = req.query.validation_status;
    const date_and_time = moment().tz(region);
    data = {
        validation_status ,
        current_time_as_per_region:`${region} => ${date_and_time}`
    }
    res.status(200).json({status:'success',data})
}

exports.get_current_time_five_zones = (req,res) =>{
    const date_and_time = moment()
    res.status(200).send(`
        Current time as per Africa/Abidjan time zone :  ${date_and_time.tz('Africa/Abidjan')}
        Current time as per America/Belem time zone :  ${date_and_time.tz('America/Belem')}
        Current time as per Asia/Kolkata time zone :  ${date_and_time.tz('Asia/Kolkata')}
        Current time as per US/Alaska time zone :  ${date_and_time.tz('US/Alaska')}
        Current time as per Singapore time zone :  ${date_and_time.tz('Singapore')}
    `);
}

